package controller;
import dao.ReadOfFile;
import dto.City;
import service.impl.FindCityImpl;
import service.impl.SortingCitiesImpl;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Menu {
    public static void controller() throws IOException {
        ReadOfFile readOfFile = new ReadOfFile();
        SortingCitiesImpl sortingCities = new SortingCitiesImpl();
        List<City> cityList = readOfFile.readAndMappingToListByNatural();
        FindCityImpl findCity = new FindCityImpl();

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int select = 0;
            if (scanner.hasNextInt()) {
                select = scanner.nextInt();
            }
            switch (select) {

                case 1:
                    readOfFile.readAndMappingToListByNatural().forEach(System.out::println);
                    break;
                case 2:
                    sortingCities.sortingByNameNotSensitive(cityList).forEach(System.out::println);
                    break;
                case 3:
                    sortingCities.sortingByNameAndDistrictWithSensRevers(cityList).forEach(System.out::println);
                    break;
                case 4:
                    Map<String, Integer> map = findCity.findCountCitiesByRegion(cityList);
                    map.entrySet().forEach(entry -> {
                        System.out.println(entry.getKey() + " " + entry.getValue());
                    });
                    break;
                case 5:
                    System.out.println(findCity.findCityWithHighestPopulation(cityList));
                    break;
                case 6:
                    scanner.close();
                    System.exit(0);
                default:
                    System.out.println("Введите значение из меню");
            }
        }
    }
}
