package service.impl;

import dto.City;
import service.SortingCities;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortingCitiesImpl implements SortingCities {

    @Override
    public List<City> sortingByNameNotSensitive(List<City> cityList) {
        Collections.sort(cityList, Comparator.comparing(City::getName, String::compareToIgnoreCase).reversed());

        return cityList;
    }

    @Override
    public List<City> sortingByNameAndDistrictWithSensRevers(List<City> cityList) {
        Collections.sort(cityList, Comparator.comparing(City::getDistrict).thenComparing(City::getName).reversed());
        return cityList;
    }
}
