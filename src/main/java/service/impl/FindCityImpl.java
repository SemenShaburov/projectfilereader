package service.impl;

import dto.City;
import service.FindCity;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FindCityImpl implements FindCity {
    @Override
    public String findCityWithHighestPopulation(List<City> cityList) {
        City[] arr = cityList.toArray(new City[0]);
        City max = arr[0];
        int currentIndex = 0;
        for (int i = 0; i < arr.length; i++) {
            if (max.getPopulation() < arr[i].getPopulation()) {
                max = arr[i];
                currentIndex = i;
            }
        }
        return "[" + currentIndex + "]" + " " + max.getPopulation();
    }

    @Override
    public Map<String, Integer> findCountCitiesByRegion(List<City> cityList) {
        return cityList.stream().collect(Collectors.toMap(City::getRegion, city -> 1, Integer::sum));
    }
}
