package service;

import dto.City;

import java.util.List;
import java.util.Map;

public interface FindCity {

    String findCityWithHighestPopulation(List<City> cityList);

    Map<String,Integer> findCountCitiesByRegion(List<City> cityList);
}
