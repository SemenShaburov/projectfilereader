package service;

import dto.City;

import java.util.List;

public interface SortingCities {
    List<City> sortingByNameNotSensitive(List<City> cityList);
    List<City> sortingByNameAndDistrictWithSensRevers(List<City> cityList);

}
