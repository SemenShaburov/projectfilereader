package dao;

import dto.City;
import dao.FileReader;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class ReadOfFile implements FileReader{

    @Override
    public List<City> readAndMappingToListByNatural() throws IOException {
        List<City> listCity = new ArrayList<>();
        Path path = Paths.get("src/main/resources/city.txt");
        Scanner scanner = new Scanner(path);
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            String[] strings = line.split(";");
            listCity.add(new City(strings[1], strings[2], strings[3], Integer.parseInt(strings[4]), strings[5]));
        }
        scanner.close();
        //Collections.sort(listCity, Comparator.comparing(City::getName));
        return listCity;
    }
}

