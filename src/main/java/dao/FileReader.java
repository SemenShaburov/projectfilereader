package dao;

import dto.City;

import java.io.IOException;
import java.util.List;

public interface FileReader {
    List<City> readAndMappingToListByNatural() throws IOException;
}
