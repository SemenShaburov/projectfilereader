import dao.ReadOfFile;
import dto.City;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import service.impl.FindCityImpl;
import service.impl.SortingCitiesImpl;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SortingCitiesImplTest {
    List<City> cityListActual;
    List<City> cityListExpect;
    List<City> cityListExpect2;
    SortingCitiesImpl sortingCities = new SortingCitiesImpl();
    ReadOfFile readOfFile = new ReadOfFile();
    FindCityImpl findCity = new FindCityImpl();

    @Before
    public void setUp() throws Exception {
        City city1 = new City("Кодинск", "Красноярский край", "Сибирский", 15653, "1978");
        City city2 = new City("Адыгейск", "Адыгея", "Южный", 12689, "1969");
        City city3 = new City("Екатеринбург", "Свердловская область", "Уральский", 1377738, "1723");
        City city4 = new City("Правдинск", " Калининградская область", "Северо-Западный", 4323, "1312");
        cityListActual = Arrays.asList(city1, city2, city3, city4);
    }

    @Test
    public void sortingByNameNotSensitive() {
        City city1 = new City("Кодинск", "Красноярский край", "Сибирский", 15653, "1978");
        City city2 = new City("Адыгейск", "Адыгея", "Южный", 12689, "1969");
        City city3 = new City("Екатеринбург", "Свердловская область", "Уральский", 1377738, "1723");
        City city4 = new City("Правдинск", " Калининградская область", "Северо-Западный", 4323, "1312");
        cityListExpect = Arrays.asList(city4, city1, city3, city2);

        Assert.assertEquals(cityListExpect, sortingCities.sortingByNameNotSensitive(cityListActual));
    }

    @Test
    public void sortingByNameAndDistrictWithSensRevers() {
        City city1 = new City("Кодинск", "Красноярский край", "Сибирский", 15653, "1978");
        City city2 = new City("Адыгейск", "Адыгея", "Южный", 12689, "1969");
        City city3 = new City("Екатеринбург", "Свердловская область", "Уральский", 1377738, "1723");
        City city4 = new City("Правдинск", " Калининградская область", "Северо-Западный", 4323, "1312");
        cityListExpect2 = Arrays.asList(city2, city3, city1, city4);

        Assert.assertEquals(cityListExpect2, sortingCities.sortingByNameAndDistrictWithSensRevers(cityListActual));

    }

    @Test
    public void findCityWithHighestPopulation() {

        Assert.assertEquals("[2] 1377738", findCity.findCityWithHighestPopulation(cityListActual));
    }

    @Test
    @Ignore
    public void findCountCitiesByRegion() {
        Map<String,Integer> map1 = new HashMap<>();
        map1.put("Красноярский край", 1);
        map1.put("Адыгея", 1);
        map1.put("Калининградская область", 1);
        map1.put("Свердловская область",1);

        Assert.assertEquals(map1, findCity.findCountCitiesByRegion(cityListActual));

    }

    @Test
    public void readAndMappingToListByNatural() throws IOException {
        cityListExpect = readOfFile.readAndMappingToListByNatural();
    }
}